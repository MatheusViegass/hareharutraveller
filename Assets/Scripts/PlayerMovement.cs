﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private CharacterController2D controller;

    private PlayerInputActions playerControls;
    private InputAction move;
    private InputAction jump;
    private InputAction run;

    float horizontalMove = 0f;
    public bool jumpPressed = false;
    public bool runPressed = false;

    private void Awake()
    {
        playerControls = new PlayerInputActions();
    }
    private void OnEnable()
    {
        move = playerControls.Player.Move;
        move.Enable();

        jump = playerControls.Player.Jump;
        jump.Enable();
        jump.performed += StartJump;
        jump.canceled += StopJump;

        run = playerControls.Player.Run;
        run.Enable();
        run.performed += StartRun;
        run.canceled += StopRun;
    }
    private void OnDisable()
    {
        move.Disable();
        jump.Disable();
        run.Disable();

    }
    void Update()
    {

        horizontalMove = move.ReadValue<float>();

        //animator.SetFloat("Speed", Mathf.Abs(horizontalMove));


    }
    private void StartJump(InputAction.CallbackContext context)
    {
        jumpPressed = true;
        controller.IsHoldingJumpingButton();
    }
    private void StopJump(InputAction.CallbackContext context)
    {
        controller.StopHoldingJumpingButton();
    }
    private void StartRun(InputAction.CallbackContext context)
    {
        runPressed = true;
    }
    private void StopRun(InputAction.CallbackContext context)
    {
        runPressed = false;
    }
    private void FixedUpdate()
    {
        //Move our character
        controller.Move(horizontalMove * Time.fixedDeltaTime, jumpPressed, runPressed);
        jumpPressed = false;

    }

    public void OnLanding()
    {
        //  animator.SetBool("IsJumping", false);
    }
}
