using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateCharacterAnimator : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private CharacterController2D characterController2D;

    private void OnEnable()
    {
        characterController2D.StateChangeAction += UpdateAnimator;
    }
    private void OnDisable()
    {
        characterController2D.StateChangeAction -= UpdateAnimator;
    }
    private void UpdateAnimator()
    {
        switch (characterController2D.GetCurrentCharacterState())
        {
            case State.IsIdle:
                SetAllAnimatorBooleansFalse();
                break;
            case State.IsJumping:
                SetAllAnimatorBooleansFalse();
                animator.SetBool("IsJumping", true);
                break;
            case State.IsFalling:
                SetAllAnimatorBooleansFalse();
                animator.SetBool("IsFalling", true);
                break;
            case State.IsWalking:
                SetAllAnimatorBooleansFalse();
                animator.SetBool("IsWalking", true);
                break;
            case State.IsRunning:
                SetAllAnimatorBooleansFalse();
                animator.SetBool("IsRunning", true);
                break;
            default:
                break;
        }       
    }
    private void SetAllAnimatorBooleansFalse()
    {
        animator.SetBool("IsJumping", false);
        animator.SetBool("IsFalling", false);
        animator.SetBool("IsWalking", false);
        animator.SetBool("IsRunning", false);
    }
}
