using System;
using UnityEngine;
using UnityEngine.Events;

public enum State
{
    IsIdle,
    IsJumping,
    IsFalling,
    IsWalking,
    IsRunning
}
public class CharacterController2D : MonoBehaviour
{
    [SerializeField] private State characterCurrentState;
    [SerializeField] private float jumpForce = 400f;                            // Amount of force added when the player jumps.
    [SerializeField] private float walkSpeed = 40f;
    [SerializeField] private float runFactorMultiplier = 1.5f;
    [SerializeField] private float jumpFactorMultiplierWhenRunning = 1.2f;
    [Range(0, .3f)] [SerializeField] private float movementSmoothing = .05f;    // How much to smooth out the movement
    [SerializeField] private bool canAirControl = false;                            // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask whatIsGround;                            // A mask determining what is ground to the character
    [SerializeField] private Transform groundCheck;                         // A position marking where to check if the player is grounded.
    [SerializeField] private Transform ceilingCheck;                            // A position marking where to check for ceilings

    const float groundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    private bool isGrounded;            // Whether or not the player is grounded.
    private bool isWalking;            // Whether or not the player is moving.
    private bool isRunning;            // Whether or not the player is moving.
    private bool isHoldingJumping;            // Whether or not the player is moving.

    [SerializeField] private float linearDrag = 4f;
    [SerializeField] private float gravity = 1f;
    [SerializeField] private float fallMultiplier = 5f;
    private float horizontalDirection;

    private Rigidbody2D rb2D;


    private bool isFacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 velocity = Vector3.zero;
    private float velocityFactor;
    private float currentSpeed;

    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;
    public Action StateChangeAction;

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        currentSpeed = walkSpeed;

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();
    }
    private void Update()
    {
        StateObserver();
    }
    private void FixedUpdate()
    {
        ModifyPhysics();

        bool wasGrounded = isGrounded;
        isGrounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                isGrounded = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }
    }

    public void Move(float move, bool jump, bool runPressed)
    {
        horizontalDirection = move;
        //only control the character if grounded or airControl is turned on
        if (isGrounded || canAirControl)
        {

            // Move the character by finding the target velocity
            if (runPressed && isGrounded) velocityFactor = 10f * currentSpeed * runFactorMultiplier;
            else if (isGrounded) velocityFactor = 10f * currentSpeed;

            Vector3 targetVelocity;
            targetVelocity = new Vector2(move * velocityFactor, rb2D.velocity.y);


            // And then smoothing it out and applying it to the character
            rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, targetVelocity, ref velocity, movementSmoothing);

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !isFacingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && isFacingRight)
            {
                // ... flip the player.
                Flip();
            }
        }
        // If the player should jump...
        if (isGrounded && jump)
        {
            // Add a vertical force to the player.
            isGrounded = false;
            rb2D.velocity = new Vector2(rb2D.velocity.x, 0);

            if (runPressed) rb2D.AddForce(Vector2.up * jumpForce * jumpFactorMultiplierWhenRunning, ForceMode2D.Impulse);
            else rb2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        if (move != 0)
        {
            if (runPressed)
            {
                isWalking = false;
                isRunning = true;
            }
            else
            {
                isWalking = true;
                isRunning = false;
            }
        }
        else
        {
            isWalking = false;
            isRunning = false;
        }
    }
    public State GetCurrentCharacterState()
    {
        return characterCurrentState;
    }
    private void StateObserver()
    {
        if (isGrounded)
        {
            ChangeState(State.IsIdle);
            if (isWalking)
            {
                ChangeState(State.IsWalking);
            }
            else if (isRunning)
            {
                ChangeState(State.IsRunning);
            }
        }

        if (rb2D.velocity.y > 0.1)
        {
            ChangeState(State.IsJumping);
        }
        if (rb2D.velocity.y < -0.1)
        {
            ChangeState(State.IsFalling);
        }
        Debug.Log(rb2D.velocity.y);
    }
    private void ChangeState(State newState)
    {
        if (characterCurrentState != newState)
        {
            characterCurrentState = newState;
            StateChangeAction?.Invoke();
        }
    }
    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        isFacingRight = !isFacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    public void IsHoldingJumpingButton()
    {
        isHoldingJumping = true;
    }
    public void StopHoldingJumpingButton()
    {
        isHoldingJumping = false;
    }
    private void ModifyPhysics()
    {
        if (isGrounded)
        {
            if (Mathf.Abs(horizontalDirection) < 0.4f)
            {
                rb2D.drag = linearDrag;
            }
            else
            {
                rb2D.drag = 0f;
            }
            rb2D.gravityScale = 0;
        }
        else
        {
            rb2D.gravityScale = gravity;
            rb2D.drag = linearDrag * 0.15f;
            if (rb2D.velocity.y < 0)
            {
                rb2D.gravityScale = gravity * fallMultiplier;
            }
            else if (rb2D.velocity.y > 0 && !isHoldingJumping)
            {
                rb2D.gravityScale = gravity * (fallMultiplier / 2);
            }

        }


    }
}
